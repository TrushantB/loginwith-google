import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View,Button } from 'react-native';
import * as Google from 'expo-google-app-auth';
import Expo from 'expo'
import * as GoogleSignIn from 'expo-google-sign-in';
import * as AppAuth from 'expo-app-auth';

const { URLSchemes } = AppAuth;
export default class App extends React.Component {
  signInWithGoogleAsync = async() => {
    1
    try {
      const result = await Google.logInAsync({
        
        // androidStandaloneAppClientId:'f43eebe220f74524cf415a64630c3c3be7f80811',
        androidClientId: '301245339810-sil76f8sci5p8levdsc09uhkeq8ecjg7.apps.googleusercontent.com',
        // iosClientId: YOUR_CLIENT_ID_HERE,
        scopes: ['profile', 'email'],
        
      });
  
      if (result.type === 'success') {
        console.log(result);
      } else {
      }
    } catch (e) {
      console.log("error",e);
    }
    //  2
    // try {
    //   const result =await GoogleSignIn.initAsync({
    //     // You may ommit the clientId when the firebase `googleServicesFile` is configured
    //     clientId: '1:301245339810:android:e6f76ce0e3bbe6fd349ffb',
    //     // Provide other custom options...
    //   });
    //   console.log(result);
      
    // } catch ({ message }) {
    //   alert('GoogleSignIn.initAsync(): ' + message);
    // }
  }

  render () {
    return (
      <View style={styles.container}>
        <Text>Open up App.js to start working on your app!</Text>
        <StatusBar style="auto" />
        <Button
            onPress={() => this.signInWithGoogleAsync()}
            title="Learn More"
            color="#841584"
            accessibilityLabel="Learn more about this purple button"
          />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
